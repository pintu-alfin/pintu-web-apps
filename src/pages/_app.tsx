import '@styles/globals.css'
import { AppProps } from 'next/app'
import Head from 'next/head'
import { useState } from 'react'
import { DehydratedState, Hydrate, QueryClient, QueryClientProvider } from '@tanstack/react-query'

type ReactQueryProps = {
  pageProps: {
    dehydratedState: DehydratedState
  }
}

function CustomApp({ Component, pageProps }: AppProps & ReactQueryProps) {
  const [queryClient] = useState(() => new QueryClient())

  return (
    <>
      <Head>
        <title>Pintu</title>
        <meta name='viewport' content='initial-scale=1, width=device-width' />
      </Head>
      <main className='app'>
        <QueryClientProvider client={queryClient}>
          <Hydrate state={pageProps.dehydratedState}>
            <Component {...pageProps} />
          </Hydrate>
        </QueryClientProvider>
      </main>
    </>
  )
}

export default CustomApp
