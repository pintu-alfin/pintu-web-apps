import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@styles/Home.module.css'
import useGetPriceChanges from '@hooks/useGetPriceChanges'
import useGetSupportedCurrencies from '@hooks/useGetSupportedCurrencies'
import { Item, ItemPrice } from 'src/types'
import { lowerCase, findLast } from 'lodash';
import numbro from '@utils/numbro'
import React from 'react'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const {
    data: listPriceChanges,
    refetch,
  } = useGetPriceChanges();
  const {
    data: listSupportedCurrencies,
  } = useGetSupportedCurrencies();
  React.useEffect(() => {
    const interval = setInterval(() => {
      refetch();
    }, 10000);
  
    return () => {
      clearInterval(interval);
    };
  }, [refetch]);
  return (
    <>
      <Head>
        <title>Aplikasi Jual Beli Bitcoin Gampang. Praktis. Instan. | Pintu</title>
        <meta name="description" content="Pintu adalah Aplikasi Jual Beli dan Investasi Bitcoin dan Aset Digital dengan Harga Final, Tanpa Komisi, Tarik Rupiah Murah. Aman Terdaftar di BAPPEBTI." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <div className='px-5 pt-3'>
          <div className={styles.containerHeader}>
            <h1 className='md:text-3xl text-l text-black font-medium'>Harga Crypto dalam Rupiah Hari Ini</h1>
            <div className="items-center flex justify-center hidden md:block" >
              <div className="relative">
                <div className="absolute top-3 left-3 items-center">
                  <svg className="w-5 h-5 text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                </div>
                <input
                  type="text"
                  className="block p-2 pl-10 w-70 text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:pl-3"
                  placeholder="Cari aset di Pintu"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="overflow-hidden w-4/5 self-center rounded-lg border border-gray-200 shadow-md m-5 p-4">
          <table className="table-auto w-full">
            <thead>
              <tr>
                <th className='text-center text-black w-1/5'>CRYPTO</th>
                <th></th>
                <th className='h-10 text-right text-black w-64'>Harga</th>
                <th className='text-center text-black w-32'>24 JAM</th>
                <th className='hidden md:table-cell text-center text-black w-32'>1 MGG</th>
                <th className='hidden md:table-cell text-center text-black w-32'>2 BLN</th>
                <th className='hidden md:table-cell text-center text-black w-32'>1 THN</th>
              </tr>
            </thead>
            <tbody>
              {listSupportedCurrencies?.data?.payload.map((item: Item, index: number) => {
                const price: ItemPrice = findLast(listPriceChanges?.data?.payload, function(n: ItemPrice) {
                  return n.pair === lowerCase(item.currencySymbol) + "/idr"
                })
                if(!price){
                  return
                }
                return(
                  <tr key={index}>
                    <td className={styles.titleContainer}>
                      <Image src={item.logo} alt='' width={24} height={24} className="mr-2"/>{item?.name}
                    </td>
                    <td className='h-10 text-lg text-center text-gray-400'>{item?.currencySymbol}</td>
                    <td className='h-10 text-lg text-right text-black' >Rp {numbro.formatCurrency(price?.latestPrice)}</td>
                    <td className={'h-10 text-lg text-center ' + (parseInt(price?.day) < 0 ? 'text-red-700' : 'text-green-700') }>{price?.day}</td>
                    <td className={'hidden md:table-cell text-lg h-10 text-center ' + (parseInt(price?.week) < 0 ? 'text-red-700' : 'text-green-700') }>{price?.week}</td>
                    <td className={'hidden md:table-cell text-lg h-10 text-center ' + (parseInt(price?.month) < 0 ? 'text-red-700' : 'text-green-700') }>{price?.month}</td>
                    <td className={'hidden md:table-cell text-lg h-10 text-center ' + (parseInt(price?.year) < 0 ? 'text-red-700' : 'text-green-700') }>{price?.year}</td>
                  </tr>
                )}
              )}
            </tbody>
          </table>
        </div>
      </main>
    </>
  )
}
