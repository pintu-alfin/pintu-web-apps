export interface ItemPrice {
  pair: string,
  latestPrice: string,
  day: string,
  week: string,
  month: string,
  year: string,
};
export interface ItemCurrencies {
  logo: string,
  currencySymbol: string,
  name: string,
  pair: string,
};

export interface Item extends ItemCurrencies, ItemPrice {
  
};

export interface ItemMenu {
  icon: string,
  title: string,
};
