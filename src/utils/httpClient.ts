import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios'

export const HttpClient = axios.create({
  timeout: 325000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Cache-Control': 'no-store'
  },
})

export interface IAPIResponse<T = any> {
  dataProduct?: any
  favorite?: any
  dataCategory?: any
  data: T
  message: string
  status: 200 | 400 | 300 | 500
}

export async function getData(url: string, config = {}) {
  return await HttpClient.get(url, { ...config }).then(response => response)
}

export async function postData(url: string, data = {}) {
  return await HttpClient.post(url, { ...data }).then(response => response)
}

export async function patchData(url: string, data = {}) {
  return await HttpClient.patch(url, { ...data }).then(response => response)
}

export async function deleteData(url: string, data = {}) {
  return await HttpClient.delete(url, { ...data }).then(response => response)
}

export async function putData(url: string, data = {}) {
  return await HttpClient.put(url, { ...data }).then(response => response)
}
const onRequest = (config: any) => {
  return config
}

const onRequestError = (error: AxiosError): Promise<AxiosError> => {
  return Promise.reject(error)
}

const onResponse = (response: AxiosResponse): AxiosResponse => {
  return response
}

const onResponseError = (error: AxiosError): Promise<AxiosError> => {
  return Promise.reject(error)
}

export function setupInterceptorsTo(axiosInstance: AxiosInstance): AxiosInstance {
  axiosInstance.interceptors.request.use(onRequest, onRequestError)
  axiosInstance.interceptors.response.use(onResponse, onResponseError)
  return axiosInstance
}

setupInterceptorsTo(HttpClient)
