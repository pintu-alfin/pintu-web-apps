import { CoreUtilsHttpClient } from '@utils/index'

export const getSupportedCurrencies = () =>
  CoreUtilsHttpClient.getData(
    'api/v2/wallet/supportedCurrencies',
  );
export const getPriceChanges = () =>
  CoreUtilsHttpClient.getData(
    'api/v2/trade/price-changes',
  );