import { useQuery } from '@tanstack/react-query'
import * as services from '@services/index'

/**
 * dok lengkap dsini https://tanstack.com/query/v4/docs/guides/queries
 * @returns
 */
const useGetSupportedCurrencies = () => {
  const data = useQuery(['get-supported-currencies'], () => {
    return services.getSupportedCurrencies();
  });
  return data;
}

export default useGetSupportedCurrencies
