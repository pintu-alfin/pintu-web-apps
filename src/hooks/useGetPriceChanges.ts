import { useQuery } from '@tanstack/react-query'
import * as services from '@services/index'

/**
 * dok lengkap dsini https://tanstack.com/query/v4/docs/guides/queries
 * @returns
 */
const useGetPriceChanges = () => {
  const data = useQuery(['get-price-changes'], () => {
    return services.getPriceChanges();
  });
  return data;
}

export default useGetPriceChanges
